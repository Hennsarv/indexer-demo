﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexeriDemo
{
    static class Program
    {
        static IEnumerable<T> MyForEach<T>(this IEnumerable<T> m, Action<T> a)
        {
            foreach (T x in m) a(x);
            return m;
        }

        static void Main(string[] args)
        {
            Inimene esiIsa = new Inimene("Adam", Sugu.Mees);
            Inimene esiEma = new Inimene("Eva", Sugu.Naine);
            var kain = Inimene.Laps(esiIsa, esiEma, "Kain", Sugu.Mees);
            var abel = Inimene.Laps(esiIsa, esiEma, "Abel", Sugu.Mees);

            Console.WriteLine($"1. {esiIsa[1].Nimi}");

            Console.WriteLine($"2. {esiIsa["Kain"].Sugu}");

            Console.WriteLine($"3. {esiIsa["Kain"]["ema"].Nimi}");

            Console.WriteLine($"4. {esiIsa.Pojad["Kain"]["ema"].Nimi}");
        }
    }

    enum Sugu { Naine, Mees, Teadmata=99}

    class Inimene
    {
        public string Nimi;
        public Sugu Sugu; // false naine, true mees

        public Inimene Isa;
        public Inimene Ema;
        public List<Inimene> Lapsed = new List<Inimene>();

        public Inimene(string nimi, Sugu sugu)
        {
            Nimi = nimi; Sugu = sugu;
        }

        public static Inimene Laps(Inimene isa, Inimene ema, string nimi, Sugu sugu)
        {
            Inimene laps = new Inimene(nimi, sugu);
            isa.Lapsed.Add(laps);
            ema.Lapsed.Add(laps);
            laps.Isa = isa;
            laps.Ema = ema;
            return laps;
        }


        public Dictionary<string, Inimene> Pojad
        {
            get => this.Lapsed.Where(x => x.Sugu == Sugu.Mees).ToDictionary(x => x.Nimi, x => x);
        }

        public Dictionary<string, Inimene> Tytred()
        {
            return this.Lapsed.Where(x => x.Sugu == Sugu.Naine).ToDictionary(x => x.Nimi, x => x);
        }


        public Inimene Laps(int mitmes)
        {
            return mitmes <= Lapsed.Count() ? Lapsed[mitmes] : null;
        }

        public Inimene Laps(string nimi)
        {
            return
        this.Lapsed
        .Where(x => x.Nimi == nimi)
        .SingleOrDefault();
        }


        public Inimene this[int mitmes]
            {
            get => mitmes <= Lapsed.Count() ? Lapsed[mitmes] : null;
            }
        public Inimene this[string nimi]
        {
            get
            {
                return 
                    nimi.ToLower() == "isa" ? this.Isa :
                    nimi.ToLower() == "ema" ? this.Ema :                    
                    this.Lapsed
                    .Where(x => x.Nimi == nimi)
                    .SingleOrDefault();
            }
        }

    }
}
